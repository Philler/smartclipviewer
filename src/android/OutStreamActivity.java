package de.smartclip.mobileSDK.reference;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import de.smartclip.mobileSDK.ScAdInfo;
import de.smartclip.mobileSDK.ScAdView;
import de.smartclip.mobileSDK.ScConfigurator;
import de.smartclip.mobileSDK.ScListener;

/**
 * Displays text with an advertisement's video that runs on top of it.
 */
public class OutStreamActivity extends BaseActivity implements ScListener {

    private ScAdView advertisement = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ScConfigurator.setOutstreamBehaviourMatrix();
        setContentView(R.layout.activity_out_stream);
        setTitle(R.string.title_outstream);
        advertisement = findViewById(R.id.advertisement);
    }

    @Override
    protected void onDestroy() {
        advertisement = null;
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Let this activity listen to ad events.
        if (advertisement != null)
            advertisement.addListener(this);
    }

    @Override
    protected void onPause() {
        // Do not forget to remove the listener you set before.
        if (advertisement != null)
            advertisement.removeListener(this);
        super.onPause();
    }

    @Override
    public boolean onClickThru(@NonNull final ScAdView scAdView, @Nullable final String s) {
        return false;
    }

    @Override
    public void onPrefetchComplete(@NonNull final ScAdView scAdView) {
        // not used here
    }
    @Override
    public void onStartCallback(@NonNull final ScAdView scAdView) {
        // not used here
    }
    @Override
    public void onEndCallback(@NonNull final ScAdView scAdView) {
        // not used here
    }
    @Override
    public void onCappedCallback(@NonNull final ScAdView scAdView) {
        // not used here
    }
    @Override
    public void onEventCallback(@NonNull final ScAdView scAdView, @NonNull final ScAdInfo scAdInfo) {
        // not used here
    }
}
