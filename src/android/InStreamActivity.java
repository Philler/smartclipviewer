package de.smartclip.mobileSDK.reference;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import de.smartclip.mobileSDK.ScAdInfo;
import de.smartclip.mobileSDK.ScAdView;
import de.smartclip.mobileSDK.ScConfigurator;
import de.smartclip.mobileSDK.ScListener;

/**
 * Displays a video in a SimpleExoPlayer that plays after an advertisement's video.
 */
public class InStreamActivity extends BaseActivity implements ScListener {

    private ScAdView adView;
    private SimpleExoPlayer player;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ScConfigurator.setInstreamBehaviourMatrix();
        setContentView(R.layout.activity_in_stream);
        setTitle(R.string.title_instream);

        // Set the ad url.
        adView = findViewById(R.id.advertisement);
        adView.setAdURL(getString(R.string.ad_url));

        // Initialize your video player
        player = initVideo(getString(R.string.demo_video_url));
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Let this activity listen to ad events.
        adView.addListener(this);
        // To avoid video and ad playing at the same time only let the video play when the ad has ended already.
        player.setPlayWhenReady(adView.hasAdEnded());
    }

    @Override
    protected void onPause() {
        // Always stop the video playback when the activity is paused.
        player.setPlayWhenReady(false);
        // Do not forget to remove the listener you set before.
        adView.removeListener(this);
        super.onPause();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onPrefetchComplete(@NonNull final ScAdView scAdView) {
        // not used here
    }

    @Override
    public void onStartCallback(@NonNull final ScAdView scAdView) {
        // not used here
    }

    @Override
    public void onEndCallback(@NonNull final ScAdView scAdView) {
        adView.destroyAd();
        adView.setVisibility(View.GONE);
        // Start the video when the ad has ended.
        player.setPlayWhenReady(true);
    }

    @Override
    public boolean onClickThru(@NonNull final ScAdView scAdView, @Nullable final String s) {
        // not used here
        return false;
    }

    @Override
    public void onCappedCallback(@NonNull final ScAdView scAdView) {
        // not used here
    }

    @Override
    public void onEventCallback(@NonNull final ScAdView scAdView, @NonNull final ScAdInfo scAdInfo) {
        // not used here
    }

    private SimpleExoPlayer initVideo(final String url) {
        final DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        final AdaptiveTrackSelection.Factory factory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        final DefaultTrackSelector defaultTrackSelector = new DefaultTrackSelector(factory);
        final SimpleExoPlayer player = ExoPlayerFactory.newSimpleInstance(this, defaultTrackSelector);

        final PlayerView videoView = findViewById(R.id.video);
        videoView.setPlayer(player);
        videoView.setUseController(false);

        final DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, "ScMobileSdkReference"), bandwidthMeter);
        final MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(url));

        player.prepare(videoSource);

        return player;
    }
}
