package de.smartclip.mobileSDK.reference;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import de.smartclip.mobileSDK.ScAdView;

/**
 * Base activity whose purpose is to handle the creation and management of the menu.
 */
abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        ScAdView.DEBUG = true;
        super.onCreate(savedInstanceState);
        setTitle(R.string.app_name_long);
    }

    @Override
    public void setTitle(final CharSequence title) {
        final android.support.v7.app.ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null)
            supportActionBar.setTitle(title);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.use_case_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_instream:
                startActivity(new Intent(this, InStreamActivity.class));
                finish();
                return true;
            case R.id.nav_outstream:
                startActivity(new Intent(this, OutStreamActivity.class));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
