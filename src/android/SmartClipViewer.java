package cordova.plugin.smartclipviewer.SmartClipViewer;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class echoes a string called from JavaScript.
 */
public class SmartClipViewer extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("test")) {
            this.test(args,callbackContext);
            return true;
        }
        return false;
    }

    private void test(JSONArray args, CallbackContext callbackContext) {
        if(args != null){
            callbackContext.success("ERFOLGREICH");
        }else{
            callbackContext.error("Please do not pass null value");
        }
    }
}
